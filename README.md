classified
==========

A potentially over-engineered class system for Lua

**Show me some docs!**

oook! [https://craftedcart.gitlab.io/lua_classified](https://craftedcart.gitlab.io/lua_classified)

**Table of contents**

- [So what do we have?](#so-what-do-we-have)
- [What don't we have?](#what-dont-we-have)
- [So how do I create a class?](#so-how-do-i-create-a-class)
- [Cool! You're using the most generic example of OOP inheritance already! Now how does inheritance work?](#cool-youre-using-the-most-generic-example-of-oop-inheritance-already-now-how-does-inheritance-work)
- [How about multiple inheritance?](#how-about-multiple-inheritance)
- [How do strict classes work?](#how-do-strict-classes-work)
- [How do those custom getter/setter property things work?](#how-do-those-custom-gettersetter-property-things-work)
- [How do I use instance_of?](#how-do-i-use-instanceof)

## So what do we have?

- Multiple inheritance
- "Strict" classes (Will throw an error when trying to read from/write to an undeclared member)
- Python-style properties with custom get/set methods
- `instance_of` / `__instance_of`
- Everything in a single file "src/class.lua"

## What don't we have?

Because y'know, it's nice to be up-front about the flaws

- Everything must be declared inside a table
  - This means function syntax is `name = function(self) ... end`<br>
  not `function Class:name() ... end`
- Abstract methods are not enforced when creating new types
- Probably some other stuff too

## So how do I create a class?

```lua
local class = require("class")

local Animal
Animal = class {
  -- Optional constructor
  __init = function(self, name)
    self.name = name
  end,

  speak = function(self)
    print("*animal noises*")
  end,
}

local a = Animal("Sam") -- Calls __init
a:speak() -- *animal noises*
```

## Cool! You're using the most generic example of OOP inheritance already! Now how does inheritance work?

```lua
local Cat
Cat = class(Animal) {
  __init = function(self, name, fur_color)
    Animal.__init(self, name) -- Call super constructor
    self.fur_color = fur_color
  end,

  speak = function(self)
    print("meow~")
  end,
}

local c = Cat("Abby", "black")
print(c.fur_color) -- black
c:speak() -- meow~
```

## How about multiple inheritance?

```lua
local Programmer
Progrmmer = class {
  has_slept = function(self)
    return false
  end
}

-- Your local catgirl programmer
local Alice
Alice = class(Cat, Programmer) {
  __init = function(self)
    Cat.__init(self, "Alice", "white")
  end,
}

local me = Alice()
print(me.fur_color) -- white
me:speak() -- meow~
print(me:has_slept()) -- false
```

## How do strict classes work?

```lua
local Vec3
Vec3 = class.strict {
  -- You *must* declare all your members in a strict class
  -- Here we set them to NULL as they're gonna be overwritten in __init
  -- anyway
  --
  -- Also you *cannot* set them to nil because Lua makes no distinction
  -- between a table value being nil, and a table value not being present
  -- at all
  x = class.NULL,
  y = class.NULL,
  z = class.NULL,

  __init = function(self, x, y, z)
    self.x = x or 0
    self.y = y or 0
    self.z = z or 0

    -- THIS WOULD ERROR as we never declared something as a member
    -- self.something = 42
  end,
}

-- Use them just like any other class
local origin_point = Vec3()
print(origin_point.x, origin_point.y, origin_point.z) -- 0 0 0

local some_place = Vec3(2.1, 8.0, -3.2)
print(some_place.x, origin_point.y, origin_point.z) -- 2.1 8.0 -3.2
```

Note that all classes that derive from a strict class must also be strict.

## How do those custom getter/setter property things work?
```lua
local class = require("class")

local City
City = class {
  -- Converts to/from the stored celsius value
  temperature_farenheit = class.property {
    get = function(self)
      return (self.temperature_celsius * 9 / 5) + 32
    end,

    set = function(self, new)
      self.temperature_celsius = (new - 32) * 5 / 9
    end,
  },

  __init = function(self)
    self.temperature_celsius = 15
  end,
}

local london = City()
print(london.temperature_celsius) -- 15
print(london.temperature_farenheit) -- 59
london.temperature_farenheit = 86
print(london.temperature_celsius) -- 30
print(london.temperature_farenheit) -- 86
```

## How do I use instance_of?
```lua
local class = require("class")

local A = class {}
local B = class {}
local C = class(A, B) {}
local D = class(C) {}

local inst = D()
-- Either one works
print(inst:__instance_of(A)) -- true
print(class.instance_of(inst, A)) -- true
```
