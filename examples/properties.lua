local class = require("class")

local City
City = class {
  -- Converts to/from the stored celsius value
  temperature_farenheit = class.property {
    get = function(self)
      return (self.temperature_celsius * 9 / 5) + 32
    end,

    set = function(self, new)
      self.temperature_celsius = (new - 32) * 5 / 9
    end,
  },

  __init = function(self)
    self.temperature_celsius = 15
  end,
}

local london = City()
print(london.temperature_celsius) -- 15
print(london.temperature_farenheit) -- 59
london.temperature_farenheit = 86
print(london.temperature_celsius) -- 30
print(london.temperature_farenheit) -- 86
